This is an API with a single endpoint to get the billing information of a PTCL consumer. Bill for a consumer is fetched by interacting programmatically with [PTCL DBill portal](https://dbill.ptcl.net.pk/PTCLSearchInvoice.aspx). The workflow listed below is vital to understand the code in this repository.

 1. First of all we make an HTTP GET request to PTCL DBill portal so we could grab the hidden form fields which were induced by ASP.NET's internal mechanisms.
 2. After we have grabbed the hidden form fields set by ASP.NET, we form a payload containing a consumer's data (phone number and account ID) alongside hidden form fields (we got in step#1) and POST it to `/PTCLSearchInvoice.aspx` hosted at `dbill.ptcl.net.pk`.
 3. If no server error occurs and payload POSTed in step#2 contained valid data, PTCL's server will send an HTTP status code of `302 Found` alongwith `set-cookie` and `location` headers in the response. These two headers are of utmost importance to us.
 4. Then we make a GET request to `location`  while sending `cookie` headers alongside (got in step#3) hosted at the same server, if this request goes successful, we will be responded with the HTML of the bill.
 5. And finally we grab the fields most-relevant to the consumer from the HTML we got in previous step using [cheerio](https://github.com/cheeriojs/cheerio), and send them back to the client as JSON.

##Usage:
A consumer has to make a GET request to the base url `bills-api.herokuapp.com/ptcl` with the query parameters `phoneNo` and `accountID`, for example:
Upon a successful request to `bills-api.herokuapp.com/ptcl?phoneNo=123456&accountID=76543`, you will get a response code of 200 with JSON response, otherwise some error code with a response if applicable.
