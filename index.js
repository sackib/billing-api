'use strict';
const express = require('express');
const ptcl = require('./routes/ptcl');
const app = express();
const port = process.env.PORT || 3000;

app.use('/ptcl', ptcl);
app.all('*', (req, res) => {
  res.status(404).json('Nothing here!');
});
app.listen(port, () => {
  console.log(`listening on port ${port}`);
});
