'use strict';
/**
 * extractCookie() takes the value of set-cookie header from the http response
 * headers as input and extracts the value of cookie from it by discarding trailing
 * cookie options
 * @param {string} setCookieHeader
 * @return {string}
 */

function extractCookie(setCookieHeader) {
  return setCookieHeader.split(';')[0];
}

module.exports = extractCookie;
