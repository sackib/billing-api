'use strict';
const cheerio = require('cheerio');
const getHTML = require('../getHTML');
const config = require('../../config');

/**
 * getHiddenInputs() makes a get request to PTCL searchInvoice form
 * located at dbill.ptcl.net.pk/PTCLSearchInvoice.aspx
 * and gets all the hidden inputs which are induced by ASP.net internal mechanisms
 * and passes them into the callback by encapsulating in a javascript object
 * @param {function} callback
 */

function getHiddenInputs(callback) {
  const options = {
    hostname: config.searchInvoice.host,
    path: config.searchInvoice.path,
    method: 'GET',
  };

  getHTML(options, (error, body) => {
    if (error) {
      callback(error);
    }
    const $ = cheerio.load(body);
    const viewState = $('#__VIEWSTATE').attr('value');
    const viewStateGenerator = $('#__VIEWSTATEGENERATOR').attr('value');
    const eventValidation = $('#__EVENTVALIDATION').attr('value');

    const hiddenInputs = { viewState, viewStateGenerator, eventValidation };
    callback(null, hiddenInputs);
  });
}

module.exports = getHiddenInputs;
