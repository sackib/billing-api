'use strict';
const https = require('https');
const querystring = require('querystring');
const getHiddenInputs = require('./getHiddenInputs');
const config = require('../../config');

/**
 * postToSearchInvoice() makes a http post request to dbill.ptcl.net.pk/PTCLSearchInvoice.aspx
 * The function gets the response headers and passes them to the callback.
 * Cookie information and location header obrained by invoking this function
 * are used to make final get request to grab the bill
 * @param {Object} inputs
 * @param {Function} callback
 */

function postToSearchInvoice(inputs, callback) {
  getHiddenInputs((error, hiddenInputs) => {
    if (error) {
      callback(error);
    }

    // serialize the postData
    const postData = querystring.stringify({
      ctl00$ContentPlaceHolder1$txtPhoneNo: inputs.phoneNo,
      ctl00$ContentPlaceHolder1$txtAccountID: inputs.accountID,
      ctl00$ContentPlaceHolder1$btnSearch: config.search,
      __VIEWSTATE: hiddenInputs.viewState,
      __VIEWSTATEGENERATOR: hiddenInputs.viewStateGenerator,
      __EVENTVALIDATION: hiddenInputs.eventValidation,
    });

    const options = {
      hostname: config.searchInvoice.host,
      path: config.searchInvoice.path,
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Content-Length': postData.length,
      },
    };

    const req = https.request(options, (res) => {
      callback(null, res.headers);
    });

    req.on('error', (error) => {
      callback(error);
    });

    req.write(postData);
    req.end();
  });
}

module.exports = postToSearchInvoice;
