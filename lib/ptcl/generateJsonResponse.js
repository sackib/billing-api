'use strict';
const cheerio = require('cheerio');

/**
 * generateJsonResponse() gets in the html of the bill scraped and returns the
 * fields which are most relevant to the consumer.
 * @param {String} html
 * @return {Object}
 */

function generateJsonResponse(html) {
  const $ = cheerio.load(html);
  const name = $('#ctl00_ContentPlaceHolder1_lblName').text();
  const billingMonth = $('#ctl00_ContentPlaceHolder1_lblBillingMonth').text();

  // little extra code after .text() is to remove time-stamp from due date string
  const dueDate = $('#ctl00_ContentPlaceHolder1_lblDueDate').text().split(' ')[0];
  const billAmount = $('#ctl00_ContentPlaceHolder1_lblPayableByDueDate').text();

  return { name, billingMonth, dueDate, billAmount };
}

module.exports = generateJsonResponse;
