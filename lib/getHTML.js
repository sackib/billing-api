'use strict';
const https = require('https');

/**
 * getHTML() makes a get requst to a secure web server and
 * passes in the resulting html to callback
 * if no error occurs
 * make sure to prefix your url by https and don't include www
 * @param {object} options
 * @param {function} callback
 */

function getHTML(options, callback) {
  const body = [];
  const req = https.request(options, (res) => {
    res.on('data', (chunk) => {
      body.push(chunk);
    });
    res.on('end', () => {
      callback(null, body.concat().toString());
    });
  });

  req.on('error', (error) => {
    callback(error);
  });

  req.end();
}

module.exports = getHTML;
