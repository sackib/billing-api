'use strict';
const express = require('express');
const router = new express.Router();
const postToSearchInvoice = require('../lib/ptcl/postToSearchInvoice');
const config = require('../config');
const extractCookie = require('../lib/extractCookie');
const getHTML = require('../lib/getHTML');
const generateJsonResponse = require('../lib/ptcl/generateJsonResponse');

router.get('/', (req, res) => {
  if (req.query.phoneNo && req.query.accountID) {
    const inputs = {
      phoneNo: req.query.phoneNo,
      accountID: req.query.accountID,
    };

    postToSearchInvoice(inputs, (error, responseHeaders) => {
      if (error) {
        console.error(`An error occured while posting to searchInvoice: ${error.message}`);
      }

      const options = {
        hostname: config.searchInvoice.host,
        path: responseHeaders.location,
        method: 'GET',
        headers: {
          cookie: extractCookie(responseHeaders['set-cookie'][0]),
        },
      };

      // if we got location headers in the response, send the final get request
      if (responseHeaders.location) {
        getHTML(options, (err, body) => {
          if (err) {
            console.error('Error occured while making the final request', err.message);
            res.status(500).json('Some server error occured');
          }
          res.json(generateJsonResponse(body));
        });
      } else {
        res.status(404).json('Record not found');
      }
    });
  } else {
    res.status(400).
    json('You need to provide both phone number and account ID in the url as query parameters');
  }
});

module.exports = router;
